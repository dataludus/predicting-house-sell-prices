# Predicting house sell prices

In this project, I'll be working with housing data for the city of Ames, Iowa, United States from 2006 to 2010. To see the project, please open the [notebook](https://gitlab.com/dataludus/predicting-house-sell-prices/-/blob/master/Basics.ipynb).

**Project Goal**

The goal of this project is to apply a machine learning workflow related to **linear regression** models (multivariate). 

The pipeline of functions that will let us quickly iterate on different models could be represented as:

<img src="pipeline.svg" />

The workflow is the following:

1. **Feature selection**: _Keep only what it's useful_
    
    1. Evaluate **correlation** between features and the target. Only features that present high correlation will be kept.
    2. Evaluate then the **correlation** between different features and recognize whether there is collinearity (redundant information) or not. Those features that present collinearity will be represented by only one of them, the most representative/usefull for our analysis.
    3. Evaluate the **variance** of the different features. Those features without enough variance are considered kind of constants and therefore will be removed

2. **Feature scaling**: _Make all features comparable_
    Each feature will be scaled from 0 to 1
3. **Feature engineering**: _Process existing features to create new ones, useful_
    1. The categorical types will not be converted to codes but instead will be converted by dummy coding. This means that for each category presented in a feature, a new feature will be generated and used as boolean. 
    2. Features with missing values will be either deleted or filled with mean values, depending in how many values are missing.